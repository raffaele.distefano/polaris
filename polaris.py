import argparse,sys,os,glob,copy,pwd
import obspy.core
import pandas
import obspy.io.sac as sac
import bisect
import numpy
from obspy import read, UTCDateTime, Stream
from obspy import Trace as tr
from obspy.core import AttribDict
from obspy.io.sac.util import SacInvalidContentError
from obspy.signal.cross_correlation import correlate_stream_template
from scipy import stats
from scipy.signal import butter,lfilter,freqz,find_peaks,detrend,welch,hilbert
from decimal import *

import plotly.graph_objects as go

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

def parseArguments():
        parser=MyParser()
        parser.add_argument('--sacfiles', help='Sac Waveforms (wildcard is allowed among \"\")')
        parser.add_argument('--snr', default='2',help='signal to noise ration')
        parser.add_argument('--plot',default='noplot',help='to plot in "browser", or in "file" or "no-plot"')
        parser.add_argument('--writesac',help='write polarity to sac file', action='store_true')
        if len(sys.argv) == 1:
            parser.print_help()
            print("Example:\n --sacfiles \"/tmp/*Z.sac\"")
            sys.exit(1)
        args=parser.parse_args()
        return args

def smooth(x,window_len=11,window='hamming'):
    """smooth the data using a window with requested size.
    
    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal 
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.
    
    input:
        x: the input signal 
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal
        
    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)
    
    see also: 
    
    numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
    scipy.signal.lfilter
 
    TODO: the window parameter could be the window itself if an array instead of a string
    NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.
    """

    if x.ndim != 1:
        raise ValueError("smooth only accepts 1 dimension arrays.")

    if x.size < window_len:
        raise ValueError("Input vector needs to be bigger than window size.")


    if window_len<3:
        return x


    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError("Window is one of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")


    s=numpy.r_[x[window_len-1:0:-1],x,x[-2:-window_len-1:-1]]
    #print(len(s))
    if window == 'flat': #moving average
        w=numpy.ones(window_len,'d')
    else:
        w=eval('numpy.'+window+'(window_len)')

    y=numpy.convolve(w/w.sum(),s,mode='valid')
    return y


def butter_bandpass(data,lowcut, highcut, fs, order):
    nyq = 0.5 * float(fs)
    low = float(lowcut) / nyq
    high = float(highcut) / nyq
    b, a = butter(order, [low, high], btype='bandpass',analog=False)
    y = lfilter(b, a, data)
    return y

def polarity(d,i1,i2,ipp,iss,mipp):
    # Creating the Pandas Data Frame to submit to find_peaks
    index=list(range(i1,i2)) # creating the list of indexes of the waveform from pre P to post P or S
    samples=list(d[i1:i2]) # cut a slice list of the waveforms based on the indexes slices
    pd_data_frame = pandas.DataFrame(samples, index , columns=['Amplitudes']) # converts the list to pandas dataframe with indexes of original waveform and a column with amplitudes
    # Selecting the Data Frame proper column
    time_series = pd_data_frame['Amplitudes'] # extracts the Amplitudes time series (with indexs)
    # Creating the pick P marker
    xP=[ipp] # gets the stable position to plot P marker
    yP=[d[ipp]] # gets stable amplitude position to plot P marker
    # Creating the pick S marker
    xS=[iss] # gets the stable position to plot S marker
    yS=[d[iss]] # gets stable the amplitudes to plot S marker
    # Creating the updated pick P index for the iterative search when amp ratio is not satisfied
    mxP=[mipp] # gets the stable position to plot P marker
    myP=[d[mipp]] # gets stable amplitude position to plot P marker
    
    # Finding Positive Peaks with find_peaks on the waveform
    indices_plus  = find_peaks(time_series)[0] # find_peaks finds max amps (peaks) indexes of the series
    peaks_plus=[time_series[index[j]] for j in indices_plus]
    index_peaks_plus=[index[j] for j in indices_plus]
 
    # Finding Negative Peaks on the negative of the waveform
    indices_minus = find_peaks(-time_series)[0] # find_peaks finds max amps (peaks) of the negativized series corresponding to the negative peaks
    peaks_minus=[time_series[index[j]] for j in indices_minus]
    index_peaks_minus=[index[j] for j in indices_minus]

    nextUP=bisect.bisect_right(numpy.asarray(index_peaks_plus),mxP) 
    nextDW=bisect.bisect_right(numpy.asarray(index_peaks_minus),mxP) 
    if index_peaks_plus[nextUP] <= index_peaks_minus[nextDW]:
         p=1
         i_sam = index_peaks_plus[nextUP]
    else:
         p=-1
         i_sam = index_peaks_minus[nextDW]
    ma=time_series.loc[[i_sam]][i_sam]
    
    ######## CHECKING the amplitude of the triggering peak VS the minmax of the noise
    # Defining minmax amps of the noise
    ni  =list(range(i1,ipp)) # creating the list of indexes of the waveform from pre P to post P or S
    ns  =list(d[i1:ipp]) # cut a slice left of P onset (noise)
    ndf = pandas.DataFrame(ns, ni , columns=['Amplitudes'])
    nts = ndf['Amplitudes'] # extracts the Amplitudes time series (with indexs)

    nip = find_peaks(nts)[0] # find_peaks finds max amps (peaks) indexes of the series
    npp = [nts[index[j]] for j in nip]
    nmap= numpy.max(numpy.asarray(npp))
    #print("Max Amp Positive",nmap)

    nim = find_peaks(-nts)[0] # find_peaks finds max amps (peaks) indexes of the series
    npm = [nts[index[j]] for j in nim]
    nmam= numpy.min(numpy.asarray(npm))
    #print("Max Amp Negative",nmam)

    mar=abs(ma)/max(abs(nmap),abs(nmam))
    #if mar <= 1.5:
    #   p=False
    return p,ma,mar,i_sam,index,index_peaks_plus,index_peaks_minus,peaks_plus,peaks_minus,xP,yP,xS,yS,time_series

def plot(wty,p,paw,xa,ya,i,ipp,ipm,pp,pm,xp,yp,xs,ys,ts,p_symbol,s_symbol,s_onset_name,sta_comp,sta_name,phw):
    # Setting up the Figure to check the result
    fig = go.Figure()
    # Plotting the Time Series
    fig.add_trace(go.Scatter(x = i,y = ts, mode = 'lines', name=wty+' Waveform'+' '+' '+tr_sac.kstnm+' '+tr_sac.kcmpnm))
    # Putting positive and negative markers
    fig.add_trace(go.Scatter(x = ipp, y = pp, mode = 'markers', marker=dict(size=5,color='green',symbol='cross'), name='Detected Positive Peaks'))
    fig.add_trace(go.Scatter(x = ipm, y = pm, mode = 'markers', marker=dict(size=5,color='orange',symbol='cross'), name='Detected Negative Peaks'))
    # Plotting Onsets
    p_onset_color = 'green' if phw == 0 or phw == 1 else 'red'
    fig.add_trace(go.Scatter(x = xp,y = yp, mode = 'markers',marker=dict(size=10,color=p_onset_color,symbol=p_symbol),name='P onset manual'))
    fig.add_trace(go.Scatter(x = xs,y = ys, mode = 'markers',marker=dict(size=10,color='blue',symbol=s_symbol),name=s_onset_name))
    # Plotting polarity pick
    if p == 1:
       pol_symbol="triangle-up"
    if p == -1:
       pol_symbol="triangle-down"
    if paw >= 1.5:
       pol_color="green"
    elif paw < 1.5:
       pol_color="red"
    fig.add_trace(go.Scatter(x = [xa],y = [ya], mode = 'markers',marker=dict(size=9,color=pol_color,line_color='red',symbol=pol_symbol),name='Polarity marker'))
    return fig
    #fig.show()
    # Now the same thing on the filtered waveform

def selewinidx(arrayofpositions):
    band_start=[]
    band_stop=[]
    arrayofpositions_sub=[]
    idsta=0
    idsto=0
    idx0 = 0
    idx1 = 1
    band_start.append(arrayofpositions[idx0]) # Store in the "good ones" the first element of the array of indexs of the over_snr frequencies
    while idx1 < len(arrayofpositions):        # ... but if the indexes are not sequential adds the idx0 to STOP and IDX1 to new start, creating parallel arrays with windows extremes
          if arrayofpositions[idx1] != arrayofpositions[idx0]+1:
             band_start.append(arrayofpositions[idx1])
             band_stop.append(arrayofpositions[idx0])
          idx0=idx1
          idx1=idx0+1
    band_stop.append(arrayofpositions[idx0])
    idmax=None
    idxBands = 0
    deltaidmax=0
    while idxBands < len(band_start):
          if (band_stop[idxBands]-band_start[idxBands]) >= deltaidmax:
             idmax = idxBands
             deltaidmax = band_stop[idxBands]-band_start[idxBands]
          idxBands = idxBands+1
    if idmax != None:
       idsta=arrayofpositions.index(band_start[idmax])
       idsto=arrayofpositions.index(band_stop[idmax])
       arrayofpositions_sub=arrayofpositions[idsta:idsto+1]
    return arrayofpositions_sub


def maxratio(r,f,snr):
    rati_val = None
    freq_val = None
    lo_co = None
    up_co = None

    over_idx=[]
    rr = r.tolist()
    for ratio in rr:
        if ratio >= snr:
#           print 'Ratio= ' + str(ratio)
           over_idx.append(rr.index(ratio))
    if len(over_idx) != 0:
           over_idx_sub=selewinidx(over_idx)
           rati_val = r[over_idx_sub]
           freq_val = f[over_idx_sub]
           lo_co = round(min(freq_val))
           up_co = round(max(freq_val))
    return lo_co,up_co,len(over_idx)

def signal_to_noise(t,fs,ne1,ne2,se1,se2,snr):
    n=t.to_obspy_trace().slice(ne1,ne2)
    n.data=detrend(n.data,type='constant')
    n.taper(max_percentage=0.05, type='cosine')

    s=t.to_obspy_trace().slice(se1,se2)
    s.data=detrend(s.data,type='constant')
    s.taper(max_percentage=0.05, type='cosine')

    nperseg1 = n.data.shape[-1]
    nperseg2 = s.data.shape[-1]
    nperseg=int(round(min([nperseg1, nperseg2])/8))

    try:
        f, Pxx_denN = welch(n.data, fs, window='hanning', nperseg=nperseg)
        f, Pxx_denS = welch(s.data, fs, window='hanning', nperseg=nperseg)
        #print Pxx_denN
        #print Pxx_denS
        #print f
        Pxx_den = numpy.divide(Pxx_denS,Pxx_denN)
    except Exception as e:
        print("Something Wrong",e)

    mna=abs(n.data).max()
    return maxratio(Pxx_den,f,snr),Pxx_den,mna

def decision_tree(p, a, smp, ar, w):
    if w >= 2:
       dp=False
       da=False
       dx=False
       di=False
       return dp,da,dx,di
    if p[0] == p[1] and p[1] == p[2]: # if all the polarities agree, other parameters are used to estimate uncertainty
       # first of all the distance among the positions where polarities are read
       d01 = abs(smp[0]-smp[1])
       d02 = abs(smp[0]-smp[2])
       d12 = abs(smp[1]-smp[2])
       if d01 <= 5 and d02 <= 5 and d12 <= 5: # if all the distances are within 5 samples full weight 1 
          imax = numpy.argmax(ar)
          dp = p[imax]
          da = a[imax]
          dx = smp[imax]
          di = imax
       else: # else polarity is not reliable
          dp=False
          da=False
          dx=False
          di=False
    else: # else polarity is not reliable 
       dp=False
       da=False
       dx=False
       di=False
    return dp,da,dx,di
       
##############################################################################################
args=parseArguments()
files=glob.glob(args.sacfiles)
snr=float(args.snr)
getcontext().prec = 3
if len(files) == 0:
   sys.stderr.write('List of files '+args.sacfiles+' is empty: please check the input\n')
   sys.exit(1)

for filesac in files:
    tr_sac = sac.SACTrace.read(filesac)
    phw = int(tr_sac.ka[3:]) if tr_sac.ka else -1
    try:
        start_time=tr_sac.reftime+tr_sac.b
        a_sec=Decimal(tr_sac.a)-Decimal(tr_sac.b)
        p_symbol='circle-open-dot'
        p_time=tr_sac.reftime+tr_sac.a
        sys.stderr.write(' '.join(("###### P manual",str(tr_sac.a),tr_sac.ka,str(filesac),'\n')))
        try:
            t0_sec=Decimal(tr_sac.t0)-Decimal(tr_sac.b)
            s_symbol='circle-open-dot'
            s_onset_name='S onset manual'
            s_time=tr_sac.reftime+tr_sac.t0
            sys.stderr.write(' '.join(("       S manual",str(t0_sec),'\n')))
        except Exception as s:
            t0_sec=Decimal(tr_sac.dist/7)+Decimal(a_sec)
            s_time=p_time+float(tr_sac.dist/7)
            s_symbol='square-open'
            s_onset_name='S onset theoretical'
            sys.stderr.write(' '.join(("       S theoretical",str(t0_sec),"(",str(s),")\n")))
    except Exception as e:
        sys.stderr.write(' '.join(("###### No P onset or error in S time",str(e),str(filesac),"\n")))
        continue
        
    # Finding the index of the P onset from SAC and defining a subset of the time series
    iP=int((Decimal(a_sec)/Decimal(tr_sac.delta))+1)
    iS=int((Decimal(t0_sec)/Decimal(tr_sac.delta))+1)
    ip1=iP-100
    ip2=iP+100
    df=1.0/tr_sac.delta

# Waveform analysis and setup of the corner frequencies
    try:
    	[lo_cof, up_cof, lenoversnr], snr_array, max_noise_abs_amp = signal_to_noise(tr_sac,df,start_time,p_time,p_time,s_time,snr)
    except Exception as e:
        lo_cof=2
        up_cof=20
        lenoversnr=False
    if lo_cof == 0:
        lo_cof=2
        up_cof=20
        lenoversnr=False

    n_plot=0
    if args.plot:
       n_plot+=1

    maxit = 3
# Working on unfiltered waveform
    wf_filt = "Unfiltered"
    sac_unfiltered=tr_sac.data #to_obspy_trace().detrend(type='linear')#.filter("bandpass",freqmin=2.0, freqmax=20.0, zerophase=True)
    ampr0=0
    nloop=1
    indexP=iP
    pol0=False
    sys.stderr.write(wf_filt)
    while ampr0 < 1.5 and nloop <= maxit:
          try:
              pol0,amp0,ampr0,xamp0,ind0,indpp0,indpm0,pp0,pm0,xp0,yp0,xs0,ys0,ts0=polarity(sac_unfiltered,ip1,ip2,iP,iS,indexP)
              sys.stderr.write(';'.join((str(nloop),str(ampr0),'\n')))
              indexP=xamp0
          except Exception as e:
              sys.stderr.write("      Polarity failed on "+wf_filt+" waveform"+str(e))
          nloop+=1
    if pol0:
       if args.plot:
          f=plot(wf_filt,pol0,ampr0,xamp0,amp0,ind0,indpp0,indpm0,pp0,pm0,xp0,yp0,xs0,ys0,ts0,p_symbol,s_symbol,s_onset_name,tr_sac.kcmpnm,tr_sac.kstnm,phw)
          if args.plot == 'file':
             fname=wf_filt+'_fig_'+str("%3.3i" % n_plot)+'.png'
             f.write_image(fname) 
          elif args.plot == 'browser':
             f.show()
    #   sys.stdout.write(' '.join(("      Station: ",tr_sac.kstnm,tr_sac.kcmpnm,wf_filt,str(pol0),str(amp0),'\n')))
    else:
        pol0 = 0
        amp0 = 0
        ampr0= 0
    #   sys.stdout.write(' '.join(("      Station: ",tr_sac.kstnm,tr_sac.kcmpnm,wf_filt,'0 0\n')))

# Working on filtered waveform
    wf_filt = "Filtered"
    #sac_bandpassed=butter_bandpass(detrend(tr_sac.data,type='constant'),lo_cof,up_cof,df,4)   
    sac_bandpassed=butter_bandpass(detrend(tr_sac.data,type='linear'),lo_cof,up_cof,df,4)   
    ampr1=0
    nloop=1
    indexP=iP
    pol1=False
    sys.stderr.write(wf_filt)
    while ampr1 < 1.5 and nloop <= maxit:
          try:
              pol1,amp1,ampr1,xamp1,ind1,indpp1,indpm1,pp1,pm1,xp1,yp1,xs1,ys1,ts1=polarity(sac_bandpassed,ip1,ip2,iP,iS,indexP)
              sys.stderr.write(';'.join((str(nloop),str(ampr1),'\n')))
              indexP=xamp1
          except Exception as e:
              sys.stderr.write("      Polarity failed on "+wf_filt+" waveform"+str(e))
          nloop+=1
    if pol1:
       if args.plot:
          f=plot(wf_filt,pol1,ampr1,xamp1,amp1,ind1,indpp1,indpm1,pp1,pm1,xp1,yp1,xs1,ys1,ts1,p_symbol,s_symbol,s_onset_name,tr_sac.kcmpnm,tr_sac.kstnm,phw)
          if args.plot == 'file':
             fname=wf_filt+'_fig_'+str("%3.3i" % n_plot)+'.png'
             f.write_image(fname) 
          elif args.plot == 'browser':
             f.show()
    #   sys.stdout.write(' '.join(("      Station: ",tr_sac.kstnm,tr_sac.kcmpnm,wf_filt,str(pol1),str(amp1)," (LowerCo, UpperCo, LenOver",str(lo_cof), str(up_cof), str(lenoversnr),')\n')))
    else:
        pol1 = 0
        amp1 = 0
        ampr1= 0
    #   sys.stdout.write(' '.join(("      Station: ",tr_sac.kstnm,tr_sac.kcmpnm,wf_filt,'0 0\n')))

# Working on smoothed waveform
    wf_filt = "Smoothed"
    sac_smooth=smooth(tr_sac.data)
    #sac_smooth=smooth(butter_bandpass(detrend(tr_sac.data,type='linear'),lo_cof,up_cof,df,4))
    ampr2=0
    nloop=1
    indexP=iP
    pol2=False
    sys.stderr.write(wf_filt)
    while ampr2 < 1.5 and nloop <= maxit:
          try:
              pol2,amp2,ampr2,xamp2,ind2,indpp2,indpm2,pp2,pm2,xp2,yp2,xs2,ys2,ts2=polarity(sac_smooth,ip1,ip2,iP,iS,indexP)
              sys.stderr.write(';'.join((str(nloop),str(ampr2),'\n')))
              indexP=xamp2
          except Exception as e:
              sys.stderr.write("Polarity failed on "+wf_filt+" waveform"+str(e))
          nloop+=1
    if pol2:
       if args.plot:
          f=plot(wf_filt,pol2,ampr2,xamp2,amp2,ind2,indpp2,indpm2,pp2,pm2,xp2,yp2,xs2,ys2,ts2,p_symbol,s_symbol,s_onset_name,tr_sac.kcmpnm,tr_sac.kstnm,phw)
          if args.plot == 'file':
             fname=wf_filt+'_fig_'+str("%3.3i" % n_plot)+'.png'
             f.write_image(fname) 
          elif args.plot == 'browser':
             f.show()
    #   sys.stdout.write(' '.join(("      Station: ",tr_sac.kstnm,tr_sac.kcmpnm,wf_filt,str(pol2),str(amp2),'\n')))
    else:
        pol2 = 0
        amp2 = 0
        ampr2= 0
    #   sys.stdout.write(' '.join(("      Station: ",tr_sac.kstnm,tr_sac.kcmpnm,wf_filt,'0 0\n')))

# Now the decision tree will decide if the polarity is reliable or not
    pol_dec, amp_dec, pos_dec, which_dec  = decision_tree([pol0, pol1,pol2], [amp0, amp1, amp2], [xamp0, xamp1, xamp2], [ampr0, ampr1, ampr2], phw)
# Screen Write
    #sys.stdout.write("Station;Location;Channel;PolarityUnfiltered;PolarityFiltered;PolaritySmoothed;AmplitudeUfiltered;AamplitudeFiltered;AmplitudeSmoothed;PolaritySampleUnfiltered;PolaritySampleFiltered;PolaritySampleSmoothed;AmpRatioUnfiltered;AmpRatioFiltered;AmpRatioSmoothed;PickW;PolarityDEC;AmplitudeDEC;PolaritySampleDEC\n")
    sys.stderr.write(';'.join((tr_sac.kstnm,tr_sac.khole,tr_sac.kcmpnm,str(pol0),str(pol1),str(pol2),str(amp0),str(amp1),str(amp2),str(xamp0),str(xamp1),str(xamp2),str(ampr0),str(ampr1),str(ampr2),str(phw),str(pol_dec),str(amp_dec),str(pos_dec),str(which_dec),"\n")))
     
# Sac Write
    if pol_dec:
       sys.stdout.write(';'.join((tr_sac.kstnm,tr_sac.khole,tr_sac.kcmpnm,str(pol0),str(pol1),str(pol2),str(amp0),str(amp1),str(amp2),str(xamp0),str(xamp1),str(xamp2),str(ampr0),str(ampr1),str(ampr2),str(phw),str(pol_dec),str(amp_dec),str(pos_dec),str(which_dec),"\n")))
       if args.writesac:
          if pol_dec == 1:
             tr_sac.ka=tr_sac.ka[:2]+'+'+tr_sac.ka[3:] if tr_sac.ka else ' P+ '
          if pol_dec == -1:
             tr_sac.ka=tr_sac.ka[:2]+'-'+tr_sac.ka[3:] if tr_sac.ka else ' P- '
          tr_sac.write(filesac)
